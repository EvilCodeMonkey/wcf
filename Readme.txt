WCF experiment

Features;
===========
This solution provides working examples and implementation notes on the following features;

ClientChannel client proxies are used throughout negating the need for SvcUtil or registering references. Heavily used in the Unit testing project this utilises the shared interface mechanism of WCF proxy generation.

Free (Almost) AsyncPattern implementation: http://ayende.com/blog/3229/wcf-async-without-proxies
Implemented with a IAsync<ServiceName> and has no explicit server or client implemnentation, Majic

Completely unhindered ( in terms of hosting / binding / addressing ) agnostic unit testing for WCF implementations.
The unit test  testBase class provides a unity injected service host, and the WCF.Service.Proxy ProxyFactory class provides a method for creating ClientChannel proxies on demand.
See the TestService class for working details.

Unity dependency injection for services leveraging Unity.WCF: http://unitywcf.codeplex.com/
See the TestBase class for working details

Fileless activation for Web hosting: http://blogs.msdn.com/b/rjacobs/archive/2010/04/05/using-system-web-routing-with-data-services-odata.aspx
See the WCF.Web.Host project for details. Impelmented purely in the Global module and hooking into the unity WcfServiceFactory 


Solution breakdown:
===================

Clients:			Contains a console client that communicates with the Web Host service (Which must also start up when it does)
Hosts:				Contains a Web Service host
Infrastructure:		Contains a simple application service class that is injected into the WCF service implementation
Service:			Contains an implementation of the Service Contract, The Contract and a common service client proxy factory.
Solution Items:		Contains this file
Testing:			Contains unit test to completey excersize the Service, operation and data contracts


Method
==========

Project setup:

A VS standard class library project [WCF.Test] was created for the Unit tests.	
A VS standard class library project [WCF.Service.Proxy] was created for the common Service.Proxy (ClientChannel).
A VS standard class library project [WCF.Contract] was created to act as the Service contract library, the IService interface was relocated to here.
A VS standard class library project [WCF.Infrastructure] was created to act as a dependent module library.
A VS standard WCF Service Library project [WCF.Library] was created to provide the service implementation.
A VS standard WCF Service application project [WCF.Web.Host] was created to servie the WCF library.
A VS standard Console project [WCF.Console.Client] was created to access the services hosted by [WCF.Web.Host]

Actions:

WCF.Test: 
---------
NUnit, Unity and Unity.WCF were added using NuGet.
A TestBase.cs class was set up to provide an injected ServiceHost and use the SerivceProxy, it also provides a hook to register and wait for asyn callbacks to ensure correct thread execution of async calls.
A TestService class derived from TestBase.cs was set up to excersize the entire Service contract. 
Each test uses the common Service and creates and closes its own client proxy from [WCF.Service.Proxy]
An App.Config was created to hold both the client and server configurations required for testing, netTcpBinding was used

WCF.Web.Host: 
-------------

Unity & Unity.WCF added via NuGet
Existing IService.cs and Service1.svc files deleted
Global.asax module addedand routing was hooked into WcfServiceFactory
WcfServiceFactory configured to register IService/Service and IDependency/Dependnecy pairs
Web.Config 
Setup to define the service address and binding defaults using wsHttpBinding.
Requries <serviceHostingEnvironment aspNetCompatibilityEnabled ="true" ...> for ServiceRoutes routing to work
Disabled mex endpoint and httpGetEnabled

References: 	 WCF.Contract , WCF.Infrastructure & WCF.Service.Proxy


WCF.Infrastructure:
-------------------
	Created an IDependecy intefcace and Dependency class.
	Made WCF.Library.Sesvice depend on this interface

References: WCF.Contract


WCF.Service.Proxy:
------------------
	Add a single class (ProxyFactory) to provide the generic IClientChannel Create<T> and Close<T> methods. 
	This is used within the test project and the WCF.Console.Client.

References: WCF.Contract & WCF.Service.Proxy


WCF.Library:
------------
	Service classes renamed from I/Service1 to I/Service
	IService moded to [WCF.Contract]
	Modified App.Config to reflect changes, ran WCF Test client and acquired Client config to use in WCF.Test and WCF.Console.Client
	Dropped app.Confif in this project (No longer needed)
	Hooked in constructor dependency to IDependency

References: 	 WCF.Contract & WCF.Infrastructure

WCF.Contract:
-------------
	Created an IAsynService interface that implements the AsyncPattern 
	Broke out each interface and class to thier own files

References: only .NET libraries

WCF.Test
--------	
Hooked IService/SErvice and IDependency/Dependency into the RegisterTypes method of TestService. 
Created app.config with config obtained from WCF test client.

References: 	 WCF.Contract , WCF.Infrastructure, WCF.Library & WCF.Service.Proxy

WCF.Console.Client
------------------

Modified program to create a synchronous and asynchronous proxy clients and excercise the service, async calls appear after 'Press any key to finish'