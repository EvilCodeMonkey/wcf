// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Contract
// File:         IService.cs
// *****************************************************************************************

using System.ServiceModel;

namespace WCF.Contract
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);
    }
}