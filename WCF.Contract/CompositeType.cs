// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Contract
// File:         CompositeType.cs
// *****************************************************************************************

using System.Runtime.Serialization;

namespace WCF.Contract
{
    [DataContract]
    public class CompositeType
    {
        [DataMember]
        public bool BoolValue { get; set; }

        [DataMember]
        public string StringValue { get; set; }
    }
}