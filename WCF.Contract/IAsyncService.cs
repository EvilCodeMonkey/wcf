﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Contract
// File:         IAsyncService.cs
// *****************************************************************************************

using System;
using System.ServiceModel;

namespace WCF.Contract
{
    [ServiceContract(Name = "IService")] // <--- spoof's this contract name to act as the named type
    public interface IAsyncService
    {
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetData(int value, AsyncCallback callback, object state);

        string EndGetData(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetDataUsingDataContract(CompositeType composite, AsyncCallback callback, object state);

        CompositeType EndGetDataUsingDataContract(IAsyncResult result);
    }
}