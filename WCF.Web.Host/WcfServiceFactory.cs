// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Web.Host
// File:         WcfServiceFactory.cs
// *****************************************************************************************

using Microsoft.Practices.Unity;
using Unity.Wcf;
using WCF.Contract;
using WCF.Infrastructure;
using WCF.Library;

namespace WCF.Web.Host
{
    public class WcfServiceFactory : UnityServiceHostFactory
    {
        protected override void ConfigureContainer(IUnityContainer container)
        {
            // register all your components with the container here
            // container
            //    .RegisterType<IService, Service>()
            //    .RegisterType<DataContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IService, Service>();
            container.RegisterType<IDependency, Dependency>();
        }
    }
}