﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Web.Host
// File:         Global.asax.cs
// *****************************************************************************************

using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using WCF.Library;

namespace WCF.Web.Host
{
    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        /// <summary>
        ///   Implements Fileless activation using ServiceRoutes by hooking into the WcfServiceFactory
        /// </summary>
        private static void RegisterRoutes()
        {
            var factory = new WcfServiceFactory();
            RouteTable.Routes.Add(new ServiceRoute("Service", factory, typeof (Service)));
        }
    }
}