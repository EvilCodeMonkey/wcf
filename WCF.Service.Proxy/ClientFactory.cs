﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Service.Proxy
// File:         ClientFactory.cs
// *****************************************************************************************

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WCF.Service.Proxy
{
    public class ProxyFactory
    {
        /// <summary>
        ///   Create an IClientChannel of the desired type
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <returns> </returns>
        /// <remarks>
        ///   Note: the internal ChannelFactory must not be closed because it disposes of the created client
        /// </remarks>
        public T Create<T>(string address, Binding binding)
        {
            var epAddress = new EndpointAddress(address);
            var factory = new ChannelFactory<T>(binding);
            var clientChannel = factory.CreateChannel(epAddress);
            Trace.WriteLine(String.Format("Created client of type {0}, Address: {1}", typeof (T).Name,
                                          epAddress.Uri.AbsolutePath));
            return clientChannel;
        }

        /// <summary>
        ///   Carefully and completely close the IClientChannel
        /// </summary>
        /// <param name="clientToClose"> </param>
        public void Close<T>(T clientToClose)
        {
            var client = clientToClose as IClientChannel;
            if (client != null)
            {
                try
                {
                    Trace.WriteLine(String.Format("Closing client on Session: {0}, Address {1}", client.SessionId,
                                                  client.RemoteAddress));
                    client.Close();
                }
                catch (CommunicationException e)
                {
                    Trace.WriteLine(
                        String.Format(
                            "Communication Exception encountered closing client on Session: {0}, Address {1}, Exception: {2}",
                            client.SessionId,
                            client.RemoteAddress, e.Message));
                    client.Abort();
                }
                catch (TimeoutException e)
                {
                    Trace.WriteLine(
                        String.Format(
                            "Timeout Exception encountered closing client on Session: {0}, Address {1}, Exception: {2}",
                            client.SessionId,
                            client.RemoteAddress, e.Message));
                    client.Abort();
                }
                catch (Exception e)
                {
                    Trace.WriteLine(
                        String.Format(
                            "Exception encountered closing client on Session: {0}, Address {1}, Exception: {2}",
                            client.SessionId,
                            client.RemoteAddress, e.Message));

                    Trace.WriteLine(String.Format("Aborting client on Session: {0}, Address {1}", client.SessionId,
                                                  client.RemoteAddress));
                    client.Abort();
                    throw;
                }
            }
        }
    }
}