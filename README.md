WCF
===

Provides;

A VS 2010 solution that provides the core of a reusable generic unit testing harness for WCF Service, Operation and Data contracts.

A Console client and Web host for the service under test.


The harness and the web host utilise the Unity IoC and Unity.Wcf to handle dependency injection for resolving service library dependencies.

The harness allows easy setup of a client-server end ot end test of the entire Service/operation/data contract under test.

This provides a host-agnostic means of testing that works transparently on any CI build server.  Note: The build server execution of such unit tests still requires elevated permissions as does any ServiceHost instantiation. Running your build server under the default Local System account will suffice.

