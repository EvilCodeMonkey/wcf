﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Library
// File:         Service.cs
// *****************************************************************************************

using System;
using System.ServiceModel.Activation;
using WCF.Contract;
using WCF.Infrastructure;

namespace WCF.Library
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service : IService
    {
        private readonly IDependency _dependency;

        public Service(IDependency dependency)
        {
            _dependency = dependency;
        }

        public string GetData(int value)
        {
            return _dependency.FormatOutput(value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }

            return _dependency.ModifyOutput(composite);
        }
    }
}