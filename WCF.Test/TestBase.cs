// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Test
// File:         TestBase.cs
// *****************************************************************************************

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.Practices.Unity;
using Unity.Wcf;
using WCF.Contract;
using WCF.Service.Proxy;

namespace WCF.Test
{
    /// <summary>
    ///   Provides a simple to use base class for WCF service unit testing Derive a class from this and pass the binding and service addresses in the default constructor
    /// </summary>
    public abstract class TestBase : IDisposable
    {
        // This is just for async service call thread control
        protected readonly SortedList<Guid, bool> AsyncCallbacksActive = new SortedList<Guid, bool>();

        protected string BindingName = "WSHttpBinding";
        protected string Address = "http://localhost:8832/Service/";

        protected readonly IUnityContainer Container;
        protected readonly UnityServiceHost Host;
        protected readonly ProxyFactory ClientFactory;

        /// <summary>
        ///   Constructor
        /// </summary>
        protected TestBase(string address = null, string bindingName = null)
        {
            ClientFactory = new ProxyFactory();
            if (address != null) Address = address;
            if (bindingName != null) BindingName = bindingName;

            // create and configure the container
            Container = new UnityContainer();

// ReSharper disable DoNotCallOverridableMethodsInConstructor
            RegisterDependencies();
// ReSharper restore DoNotCallOverridableMethodsInConstructor


            // create the host and open it
            Host = new UnityServiceHost(Container, Container.Resolve<IService>().GetType());
            try
            {
                Host.Open();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }

        /// <summary>
        ///   Overridable method to affect container configuration
        /// </summary>
        protected abstract void RegisterDependencies();

        /// <summary>
        ///   Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            // Note: using _host.Close() with WsHttpBinding adds 10 seconds to the test run so we just Abort in those circumstances...
            Host.Close();
        }

        /// <summary>
        ///   Registers a new callback and marks it as active
        /// </summary>
        /// <returns> </returns>
        protected Guid RegisterWaitForAsyncCallback()
        {
            var id = Guid.NewGuid();
            AsyncCallbacksActive.Add(id, true);
            return id;
        }

        /// <summary>
        ///   Closes an existing async callback
        /// </summary>
        /// <param name="id"> </param>
        protected void CloseWaitForAsyncCallback(Guid id)
        {
            AsyncCallbacksActive[id] = false;
        }

        /// <summary>
        ///   Makes the thread wait for an async callback to complete
        /// </summary>
        /// <param name="id"> </param>
        protected void WaitForAsyncCallBack(Guid id)
        {
            while (AsyncCallbacksActive[id])
            {
                Thread.Sleep(10);
            }
            AsyncCallbacksActive.Remove(id);
        }
    }
}