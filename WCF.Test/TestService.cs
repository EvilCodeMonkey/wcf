﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Test
// File:         TestService.cs
// *****************************************************************************************

using System.ServiceModel;
using Microsoft.Practices.Unity;
using NUnit.Framework;
using WCF.Contract;
using WCF.Infrastructure;

namespace WCF.Test
{
    /// <summary>
    ///   Tests the library implementation and the Service, Operation and Data contracts in a self hosted Dependency injected environment
    /// </summary>
    [TestFixture]
    public class TestService : TestBase
    {
        /// <summary>
        ///   Constructor which modifies the base binding and addressing parameters
        /// </summary>
        public TestService() : base("net.Tcp://localhost:7659", "netTcpBinding")
        {
        }

        /// <summary>
        ///   Configures the underlying container for use by these tests
        /// </summary>
        protected override void RegisterDependencies()
        {
            Container.RegisterType<IService, Library.Service>();
            Container.RegisterType<IDependency, Dependency>();
        }


        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }


        [Test]
        public void TestThatCallingGetDataWorksAsExpected()
        {
            const int value = 10;

            var client = ClientFactory.Create<IService>(Address, new NetTcpBinding(BindingName));
            var output = client.GetData(value);

            Assert.AreEqual(string.Format("You entered: {0}", value), output);

            ClientFactory.Close(client);
        }

        [Test]
        public void AsyncTestThatCallingGetDataWorksAsExpected()
        {
            var client = ClientFactory.Create<IAsyncService>(Address, new NetTcpBinding(BindingName));


            const int value = 10;
            var id = RegisterWaitForAsyncCallback();
            {
                client.BeginGetData(value,
                                    result =>
                                        {
                                            var newValue = client.EndGetData(result);
                                            Assert.AreEqual(string.Format("You entered: {0}", value),
                                                            newValue);
                                            CloseWaitForAsyncCallback(id);
                                        },
                                    null);
            }

            WaitForAsyncCallBack(id);

            ClientFactory.Close(client);
        }


        [Test]
        public void TestThatCallingGetDataUsingDataContractWithNullThrowsException()
        {
            var client = ClientFactory.Create<IService>(Address, new NetTcpBinding(BindingName));

            Assert.Throws<FaultException<ExceptionDetail>>(() => client.GetDataUsingDataContract(null));

            ClientFactory.Close(client);
        }


        [Test]
        public void TestThatCallingGetDataUsingDataContractWorksAsExpected()
        {
            var client = ClientFactory.Create<IService>(Address, new NetTcpBinding(BindingName));

            var value = new CompositeType {BoolValue = false, StringValue = "input"};
            var output = client.GetDataUsingDataContract(value);

            Assert.AreEqual(true, output.BoolValue);
            Assert.AreEqual(string.Format("{0} Suffix", value.StringValue), output.StringValue);

            ClientFactory.Close(client);
        }

        [Test]
        public void AsyncTestThatCallingGetDataUsingDataContractWorksAsExpected()
        {
            var client = ClientFactory.Create<IAsyncService>(Address, new NetTcpBinding(BindingName));

            var value = new CompositeType {BoolValue = false, StringValue = "input"};

            var id = RegisterWaitForAsyncCallback();

            client.BeginGetDataUsingDataContract(value, result =>
                                                            {
                                                                var output =
                                                                    client.EndGetDataUsingDataContract(
                                                                        result);
                                                                Assert.AreEqual(true, output.BoolValue);
                                                                Assert.AreEqual(
                                                                    string.Format("{0} Suffix", value.StringValue),
                                                                    output.StringValue);
                                                                CloseWaitForAsyncCallback(id);
                                                            }, null);

            WaitForAsyncCallBack(id);
            ClientFactory.Close(client);
        }
    }
}