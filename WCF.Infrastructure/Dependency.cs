// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Infrastructure
// File:         Dependency.cs
// *****************************************************************************************

using WCF.Contract;

namespace WCF.Infrastructure
{
    /// <summary>
    ///   Implements a simple dependency class for use by the service which is to be injected
    /// </summary>
    public class Dependency : IDependency
    {
        public string FormatOutput(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType ModifyOutput(CompositeType value)
        {
            value.BoolValue = true;
            value.StringValue += " Suffix";
            return value;
        }
    }
}