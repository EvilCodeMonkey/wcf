﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Infrastructure
// File:         IDependency.cs
// *****************************************************************************************

using WCF.Contract;

namespace WCF.Infrastructure
{
    public interface IDependency
    {
        string FormatOutput(int value);
        CompositeType ModifyOutput(CompositeType value);
    }
}