﻿// *****************************************************************************************
// Solution:     WCF
// Project:      WCF.Console.Client
// File:         Program.cs
// *****************************************************************************************

using System.ServiceModel;
using WCF.Contract;
using WCF.Service.Proxy;

namespace WCF.Console.Client
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var p = new Program();
            p.Run(args);
        }


        private ProxyFactory _clientFactory;

        public void Run(string[] args)
        {
            System.Console.WriteLine("Ensure the service is running then press any key to start");
            System.Console.ReadKey(true);

            _clientFactory = new ProxyFactory();

            var syncClient = _clientFactory.Create<IService>("http://localhost:29087/Service/",
                                                             new WSHttpBinding("WSHttpBinding"));
            var asyncClient = _clientFactory.Create<IAsyncService>("http://localhost:29087/Service/",
                                                                   new WSHttpBinding("WSHttpBinding"));


            var result1 = syncClient.GetData(10);
            var result2 =
                syncClient.GetDataUsingDataContract(new CompositeType {BoolValue = false, StringValue = "Hello"});
            System.Console.WriteLine(string.Format("GetData returned: {0}", result1));
            System.Console.WriteLine(string.Format("GetDataUsingDataContract returned: {0} and {1}", result2.BoolValue,
                                                   result2.StringValue));

            _clientFactory.Close(syncClient);

            asyncClient.BeginGetData(20,
                                     result => System.Console.WriteLine(
                                         string.Format("BeginGetData returned: {0}", asyncClient.EndGetData(result))
                                                   ), null);

            asyncClient.BeginGetDataUsingDataContract(
                new CompositeType {BoolValue = false, StringValue = "Hello Again"},
                result =>
                    {
                        var response = asyncClient.EndGetDataUsingDataContract(result);
                        System.Console.WriteLine(
                            string.Format("BeginGetDataUsingDataContract returned: {0}, {1}", response.BoolValue,
                                          response.StringValue));
                    }, null);

            System.Console.WriteLine("Press any key to finish");
            System.Console.ReadKey(true);
            _clientFactory.Close(asyncClient);
        }
    }
}